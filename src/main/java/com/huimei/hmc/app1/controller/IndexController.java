package com.huimei.hmc.app1.controller;

import com.huimei.hmc.upms.client.common.base.BaseUpmsController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by huangyuheng on 2017/11/8.
 */
@RestController
@RequestMapping("index")
public class IndexController extends BaseUpmsController {

    @RequestMapping(value = "")
    public String index(HttpServletRequest request, HttpServletResponse response){

        return "app1/index";
    }

    @RequiresPermissions("app1:index:app1")
    @RequestMapping(value = "app1")
    public String app1(HttpServletRequest request, HttpServletResponse response){

        return "app1";
    }

}
